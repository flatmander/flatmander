#!/usr/bin/python

import numpy as np
import scipy.interpolate
import scipy.integrate
import pylab as P
import fist
import experiment
import cProfile
import time


def profile(c):
    bb = np.linspace(0.0, c.bmax, 100) # Range of impact parameters
    rr = bb * c.r500
    ysz = map(lambda b: scipy.integrate.quad(c._ig_tsz, b+0.0001,
                                        c.bmaxc, args=(b,))[0], bb)
    fac_ysz = (2. * 2. * 2.051 / 511.) * c.r500
    g_nu = 1.
    ysz = g_nu * fac_ysz * np.array(ysz)
    interp = scipy.interpolate.interp1d( rr, ysz, kind='quadratic', 
                                         bounds_error=False, fill_value=0.0 )
    return interp


def profile2(c, itype='linear'):
    bb = np.linspace(0.0, c.bmax, 100) # Range of impact parameters
    rr = bb * c.r500
    
    # Interpolate P
    _r = np.logspace(-3., np.log10(c.bmax*c.r500), 500)
    _P = c.P(_r)
    Pinterp = scipy.interpolate.interp1d(_r, _P, kind='linear', bounds_error=False, fill_value=0.)
    r500 = c.r500
    
    ig_tsz = lambda x, b: Pinterp(x*r500) * (x / np.sqrt(x**2. - b**2.))
    
    _x = [np.logspace(np.log10(b+0.0001), np.log10(c.bmaxc), 1200) for b in bb]
    ysz = [scipy.integrate.simps(ig_tsz(_x[i], bb[i]), _x[i]) for i in range(bb.size)]

    fac_ysz = (2. * 2. * 2.051 / 511.) * c.r500
    g_nu = 1.
    ysz = g_nu * fac_ysz * np.array(ysz)
    interp = scipy.interpolate.interp1d( rr, ysz, kind=itype, 
                                         bounds_error=False, fill_value=0.0 )
    return interp


# Load a cluster
p = experiment.p
cosmo = experiment.cosmo
template = fist.litemap_from_fits("sims/map/data_01.fits")
cluster_set = fist.ClusterSet(catalogue=p['clusterFile'], map_template=template)
c = cluster_set.clusters[0]

r = np.logspace(-4., np.log10(30.*c.r500), 10000)


# ORIGINAL
t = time.time()
prof1 = profile(c)
print "Original:", round(time.time()-t, 4), "sec."


t = time.time()
prof1 = profile2(c, itype='linear')
prof2 = profile2(c, itype='quadratic')
print "New ver.:", round(time.time()-t, 4), "sec."


m = cluster_set.get_cluster_map(0)
#P.matshow(np.log10(m))
P.plot(m[128])
P.plot(m[130])
P.plot(m[132])
#P.colorbar()
P.show()
exit()


# Plot results
prof1 = prof1(r)
prof2 = prof2(r)
P.subplot(111)
#P.plot(r, prof(r), 'k-')
P.plot(r, prof1/np.max(prof1), 'k-')
P.plot(r, prof2/np.max(prof2), 'r-')
P.axhline(0., ls='solid')
P.yscale('log')
#P.xscale('log')
P.show()
