from flipper import *
import gibbs_tools
import precond
import ClusterSet
from mpi4py import MPI

# (For testing)
RESCALE_CLUSTER_SIZE = 1. # Enlarge cluster angular size by this factor
RESCALE_TSZ_AMP = 5. # Scale cluster TSZ signal amplitude by this factor
FREQ = 150. # Instrumental frequency

RAD_TO_MIN = 180/numpy.pi*60

sample_cluster_amps = True

p = { 'raSizeDeg':       4.27,
    'decSizeDeg':      4.27,
    'pixScaleXarcmin': 1,
    'pixScaleYarcmin': 1,
    'rmsArcmin':       15.,
    'beamFile':        'beams_AR1_2009_120815.dat',
    'theoryFile':      'bode_almost_wmap5_lmax_1e4_lensedCls.dat',
    'ReaStart':0,
    'ReaStop':1,
    'iStart': 0,
    'iStop': 10,
    'epsilon': 1e-13,
    'plotInterval':100,
    'imax':350
}   

cosmo = {
    'omega_M_0': 		0.3,
    'omega_lambda_0': 	0.7,
    'omega_b_0': 		0.045,
    'omega_n_0':		0.0,
    'omega_k_0':		0.0,
    'N_nu':			0,
    'h':				0.7,
    'n':				0.96,
    'sigma_8':			0.8,
    'baryonic_effects': True
}

# Settings
ReaStart=p['ReaStart'] # No. CMB realisations
ReaStop=p['ReaStop']
iStart= p['iStart'] # No. Gibbs steps (for each realisation)
iStop= p['iStop']
rmsArcmin=p['rmsArcmin'] # Noise per arcmin

# Set-up MPI system
# Will distribute sampling amongst lots of independent nodes (for now, 
# amplitude sampling steps are independent => no Markov chain required)
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
print "MPI rank, size:", rank, size
delta = (ReaStop - ReaStart)/size
if delta == 0: raise ValueError, 'Too many processors for too small a loop!'

# Decide when to stop making new realisations
iMin = ReaStart+rank*delta
iMax = ReaStart+(rank+1)*delta
if iMax>ReaStop:
	iMax = ReaStop
elif (iMax > (ReaStop - delta)) and iMax <ReaStop:
	iMax = ReaStop

# Make directories for maps, results etc.
mapDir = 'map'
clusterDir = 'cluster'
try:
    os.makedirs(mapDir)
except:
    pass
try:
    os.makedirs(clusterDir)
except:
    pass
try:
    os.makedirs(mapDir+'/mapPlots')
except:
    pass


# Initialize theory Cl's and beam
l, cl_TT = numpy.loadtxt(p['theoryFile']).T[:2]
cl_TT = cl_TT*2*numpy.pi/(l*(l+1))
ell, bl = numpy.loadtxt(p['beamFile']).T

# Generate flat-sky template
# FIXME: Coords are fixed for now
T_map = gibbs_tools.makeEmptyCEATemplate(
                p['raSizeDeg'], p['decSizeDeg'],
                meanRa=180., meanDec=0.,
                pixScaleXarcmin = p['pixScaleXarcmin'],
                pixScaleYarcmin= p['pixScaleYarcmin'] )
print "="*50
print "MAP PROPERTIES"
T_map.info()
print "="*50

# Generate 2D power spectrum from Cl's, set monopole to zero, and make 2D beam template
power_2d = gibbs_tools.make2dPowerSpectrum(T_map, l, cl_TT)
power_2d[0,0] = 1e-10
beamTemp = gibbs_tools.makeTemplate(T_map, bl, ell, numpy.max(l)-2, outputFile = None)
beamTemp = beamTemp.data[:]

# Set cluster map to zero (useful if sample_cluster_amps=False)
cluster_map = 0.

# Set mask to 1 everywhere (no masking)
mask = T_map.copy()
mask.data[:] = 1

# Build noise covmat
pixArea = RAD_TO_MIN**2. * T_map.pixScaleX * T_map.pixScaleY
InvNoiseCov = T_map.copy()
InvNoiseCov.data[:] = (pixArea)/(rmsArcmin)**2. # Noise per px

# MAIN LOOP: Through CMB realisations
for k in xrange(iMin,iMax):

    # Generate an realisation of the CMB
    data = numpy.random.randn(T_map.Ny, T_map.Nx)
    a_l = numpy.fft.fft2(data)
    a_l *= numpy.sqrt(power_2d)
    data = numpy.real(numpy.fft.ifft2(a_l))
    T_map.data[:] = data
    
    # Plot result and save to PNG
    gibbs_tools.plot(T_map.data, mapDir+'/mapPlots/'+'InitialCMB_%03d.png'%k)

    # Add cluster to sky map and initialise cluster template
    if sample_cluster_amps:
        cluster_set = ClusterSet.ClusterSet(catalogue="data/MCXC_1clu.dat", map_template=T_map)
        cluster_map = cluster_set.get_cluster_map(0, rescale=RESCALE_CLUSTER_SIZE)
        g_nu = cluster_set.tsz_spectrum(nu=FREQ)
        cluster_map *= g_nu
        
        # Add amplitude-scaled cluster to map (and then plot)
        T_map.data += RESCALE_TSZ_AMP * cluster_map
        gibbs_tools.plot(RESCALE_TSZ_AMP * cluster_map, 
                         mapDir+'/mapPlots/'+'Initialcluster_%03d.png'%k)

    # Convolve sky map with the beam
    T_map = gibbs_tools.applyBeam(T_map, beamTemp)

    # Add noise to map, and multiply by mask
    T_map = gibbs_tools.addWhiteNoise(T_map, rmsArcmin)
    T_map.data[:] *= mask.data[:]
    InvNoiseCov.data[:] *= mask.data[:]

    # Output FITS file and plots
    T_map.writeFits(mapDir+os.path.sep+'data_%03d.fits'%k, overWrite=True)
    T_plot = T_map.data[:] * (1+numpy.log(mask.data[:])) # Useful for seeing masked regions
    gibbs_tools.plot(T_plot,mapDir+'/mapPlots/'+'Initialdata_%03d.png'%k)
    
    # BEGIN GIBBS SAMPLER
    cluster_amp_array=[]
    for i in range(iStart,iStop):
        print "\n* Gibbs step", i, "\n"
    
        # Initial guess for CMB and cluster amplitudes
        # In Commander notation (Eriksen et al. 2008, Eq. 22)
        x = precond.computeX0(T_map)
        
        # b is the data vector, plus white noise realisation, projection factors etc.
        # (See Eriksen et al. Eq. 22)
        b = precond.computeB(T_map, InvNoiseCov, beamTemp, power_2d, cluster_map)

        # Solve system using Conjugate Gradient method
        # (Return only cluster amplitude sample as result)
        t = time.time()
        cluster_amp = precond.preCondConjugateGradientSolver(k, i, b, x,
                         power_2d, beamTemp, InvNoiseCov, cluster_map,
                         eps=p['epsilon'], i_max=p['imax'],
                         plotInterval=p['plotInterval'],
                         mapDir=mapDir )
        print "Time for Gibbs step:", round(time.time()-t, 3), "sec."
        
        # Add cluster amplitude sample to array
        cluster_amp_array += [cluster_amp]
    
    # Write Gibbs samples to file
    g = open('cluster/cluster_amp_%03d_all.dat'%k, mode="w")
    for l in xrange(len(cluster_amp_array)):
        g.write("%f\n"%(cluster_amp_array[l]))
    g.close()

