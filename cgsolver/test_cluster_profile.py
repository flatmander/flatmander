#!/usr/bin/python

import numpy as np
import pylab as P
from cluster_profile import ArnaudProfile

# Create new cluster profile with "illustrative" parameters.
p = ArnaudProfile()

# Output interpolation function for TSZ profile at 143 GHz
# To convert into angle: theta = arctan(r / d_A(z))
r = np.logspace(-3., np.log10(30), 300)
interp_tsz_143 = p.tsz_profile(nu=143.)
interp_tsz_200 = p.tsz_profile(nu=200.)
interp_tsz_350 = p.tsz_profile(nu=350.)

# Plot cluster TSZ profile as a function of radius
P.subplot(111)

P.plot(r, interp_tsz_143(r))
P.plot(r, interp_tsz_200(r))
P.plot(r, interp_tsz_350(r))

P.axhline(0., color='k', ls='dotted')
P.xscale('log')
P.xlabel("r [Mpc]")
P.ylabel("TSZ ($\Delta T/T$)")
P.show()
