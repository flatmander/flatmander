#!/usr/bin/python
"""
Create a contrained realisation of the CMB, with a cluster somewhere.
"""
import numpy as np
from flipper import *
import pyfits
import gibbs_tools
import pylab as P
import ClusterSet

# (For testing)
RESCALE_CLUSTER_SIZE = 5. # Enlarge cluster angular size by this factor
RESCALE_TSZ_AMP = 30. # Scale cluster TSZ signal amplitude by this factor

p = { 'raSizeDeg':       5.,
      'decSizeDeg':      5.,
      'pixScaleXarcmin': 0.5,
      'pixScaleYarcmin': 0.5,
      'rmsArcmin':       3.,
      'beamFile':        'beams_AR1_2009_120815.dat',
      'theoryFile':      'bode_almost_wmap5_lmax_1e4_lensedCls.dat'
}

cosmo = {
 'omega_M_0': 		0.3,
 'omega_lambda_0': 	0.7,
 'omega_b_0': 		0.045,
 'omega_n_0':		0.0,
 'omega_k_0':		0.0,
 'N_nu':			0,
 'h':				0.7,
 'n':				0.96,
 'sigma_8':			0.8,
 'baryonic_effects': True
}

# Load theoretical power spectrum
theoryFile = p['theoryFile']
data = np.loadtxt(theoryFile)
l = data[:,0]
cl = data[:,1]
cl *= 2.*np.pi / (l*(l+1))

# Create empty map for patch of sky
T_map = gibbs_tools.makeEmptyCEATemplate(
                   p['raSizeDeg'], p['decSizeDeg'],
                   meanRa = 180., meanDec = 0., 
                   pixScaleXarcmin = p['pixScaleXarcmin'],
                   pixScaleYarcmin= p['pixScaleYarcmin'] )

# Load beam
l, bl = np.loadtxt(p['beamFile'], unpack=True)
nl = bl*0 + (p['rmsArcmin']*np.pi/(180*60.))**2
beam_template = gibbs_tools.makeTemplate(T_map, bl, l, np.max(l)-2, outputFile=None)

# Load clusters from catalogue
cluster_set = ClusterSet.ClusterSet(catalogue="data/MCXC_1clu.dat", map_template=T_map)

# Do constrained realisation of CMB
T_map.fillWithGaussianRandomField(l, cl, bufferFactor=1)
T_map.data[:] -= np.mean(T_map.data[:])
cmb_map = T_map.data.copy()

# Add (rescaled) cluster signal to map
cluster_map = cluster_set.get_cluster_map(0, rescale=RESCALE_CLUSTER_SIZE) # TSZ map
g_nu = cluster_set.tsz_spectrum(nu=150.) # Frequency-dependent factor

T_map.data += RESCALE_TSZ_AMP * g_nu * cluster_map

# Convolve with beam and then add white noise
T_map = gibbs_tools.applyBeam(T_map, beam_template)
T_map = gibbs_tools.addWhiteNoise(T_map, p['rmsArcmin'])

# Plot total temperature, CMB, and TSZ maps
mmin = -300.; mmax = 300.
print mmin, mmax
P.subplot(131)
#T_map.plot(show=False, valueRange=[-300., 300.])
P.imshow(T_map.data, vmin=mmin, vmax=mmax)
P.subplot(132)
P.imshow(cmb_map, vmin=mmin, vmax=mmax)
P.subplot(133)
P.imshow(cluster_map)
P.show()
