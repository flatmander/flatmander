#!/usr/bin/python
"""
Test constrained realisation of CMB on a flat patch of sky.
"""

import numpy as np
from flipper import *
import pyfits
import gibbs_tools
import astLib.astWCS
import pylab as P

patchDir = 'patches'
specDir = 'spectra'

# Read settings from file
p = flipperDict.flipperDict()
if len(sys.argv) > 1:
  p.read_from_file(sys.argv[1])
else:
  print "ERROR: No settings dictionary file specified. Exiting."

# Create empty map for patch of sky
temp = gibbs_tools.makeEmptyCEATemplate(
                   p['raSizeDeg'], p['decSizeDeg'],
                   meanRa = 180., meanDec = 0., 
                   pixScaleXarcmin = p['pixScaleXarcmin'],
                   pixScaleYarcmin= p['pixScaleYarcmin'] )

# Load theoretical power spectrum
theoryFile = p['theoryFile']
data = np.loadtxt(theoryFile)
l = data[:,0]
cl = data[:,1]
cl * =2*np.pi / (l*(l+1))

# Load beam
l, bl = np.loadtxt(p['beamFile'], unpack=True)
nl = bl*0 + (p['rmsArcmin']*numpy.pi/(180*60.))**2
beamTemp = gibbs_tools.makeTemplate(temp, bl, l, numpy.max(l)-2, outputFile=None)

# Create output directories, as needed
try:
    os.makedirs(patchDir)
except:
    pass
try:
    os.makedirs(patchDir+'/mapPlots')
except:
    pass
try:
    os.makedirs(specDir)
except:
    pass


# Loop to create nRea realisations of the CMB
cl_all = []
for i in range(p['nRea']):
    # Do constrained realisation of CMB
    T_map = temp.copy()
    T_map.fillWithGaussianRandomField(l, cl, bufferFactor=1)
    T_map.data[:] -= np.mean(T_map.data[:])
    
    # Convolve with beam and then add white noise
    T_map = gibbs_tools.applyBeam(T_map, beamTemp)
    T_map = gibbs_tools.addWhiteNoise(T_map, p['rmsArcmin'])
    
    # Plot resulting temperature map
    T_map.plot(show=False)
    P.savefig(patchDir+os.path.sep+'mapPlots/T_map_%03d.png'%(i))
    P.clf()

    # Calculate binned power spectrum of the realised map
    p2d = fftTools.powerFromLiteMap(T_map)
    _bins = p2d.binInAnnuli(p['binningFile'])
    binLower, binUpper, binCenter, binMean, binSd, binWeight = _bins
    
    # Output binned power spectrum
    fileName = specDir + os.path.sep + 'cl_TT_%03d.dat'%i
    gibbs_tools.writeBinnedSpectrum(binCenter, binMean, fileName)
    
    # Add Cl's for this realisation to total
    cl_all += [binMean]

# Find mean and std. dev. 
cl_mean = np.mean(cl_all,axis=0)
cl_std = np.std(cl_all,axis=0)

# Plot average binned power spectrum from all realisations
P.semilogy()
P.plot(binCenter, cl_mean*binCenter**2, 'o')
P.plot(l, (cl*bl**2+nl)*l**2)
P.xlim(0, 9e3)
P.ylim(1e-2, 1e5)
P.show()
