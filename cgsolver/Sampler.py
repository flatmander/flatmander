from flipper import *
import gibbs_tools
import ClusterSet
import precond



# (For testing)
RESCALE_CLUSTER_SIZE = 1. # Enlarge cluster angular size by this factor
RESCALE_TSZ_AMP = 5. # Scale cluster TSZ signal amplitude by this factor

p = { 'raSizeDeg':       4.27,
    'decSizeDeg':      4.27,
    'pixScaleXarcmin': 0.5,
    'pixScaleYarcmin': 0.5,
    'rmsArcmin':       15.,
    'beamFile':        'beams_AR1_2009_120815.dat',
    'theoryFile':      'bode_almost_wmap5_lmax_1e4_lensedCls.dat',
    'iStart': 0,
    'iStop': 150,
    'epsilon': 10**(-13),
    'plotInterval':100,
    'imax':350
    
}   

cosmo = {
    'omega_M_0': 		0.3,
    'omega_lambda_0': 	0.7,
    'omega_b_0': 		0.045,
    'omega_n_0':		0.0,
    'omega_k_0':		0.0,
    'N_nu':			0,
    'h':				0.7,
    'n':				0.96,
    'sigma_8':			0.8,
    'baryonic_effects': True
}


iStart= p['iStart']
iStop= p['iStop']
rmsArcmin=p['rmsArcmin']


mapDir='map'
clusterDir='cluster'


try:
    os.makedirs(mapDir)
except:
    pass


try:
    os.makedirs(clusterDir)
except:
    pass

try:
    os.makedirs(mapDir+'/mapPlots')
except:
    pass



#Initialize Theory and beam
data = numpy.loadtxt(p['theoryFile'])
l = data[:,0]
cl_TT = data[:,1]
cl_TT=cl_TT*2*numpy.pi/(l*(l+1))


ell, bl = numpy.transpose(numpy.loadtxt(p['beamFile']))


#Generate template, pixel space noise and normalization

T_map=gibbs_tools.makeEmptyCEATemplate(p['raSizeDeg'], p['decSizeDeg'],meanRa = 180., meanDec = 0., pixScaleXarcmin = p['pixScaleXarcmin'], pixScaleYarcmin= p['pixScaleYarcmin'])

T_map.info()




#Simulate Fake data


#T_map=gibbs_tools.fillWithGaussianRandomField(T_map,l,cl_TT,bufferFactor = 1)


#T_map.data-=numpy.mean(T_map.data)
#T_map.plot()




# Generate 2d template and Mask

power_2d=gibbs_tools.make2dPowerSpectrum(T_map,l,cl_TT)





power_2d[0,0]=10**(-10)
data=numpy.random.randn(T_map.Ny,T_map.Nx)
a_l=numpy.fft.fft2(data)
a_l*=numpy.sqrt(power_2d)
data=numpy.real(numpy.fft.ifft2(a_l))
T_map.data[:]=data



beamTemp=gibbs_tools.makeTemplate(T_map, bl, ell, numpy.max(l)-2, outputFile = None)
beamTemp=beamTemp.data[:]

mask=T_map.copy()
mask.data[:]=1
#mask.data[50:100,50:100]=0



#Generate Noise Covariance with proper normalization

radToMin = 180/numpy.pi*60
pixArea = radToMin**2 * T_map.pixScaleX*T_map.pixScaleY


InvNoiseCov=T_map.copy()
InvNoiseCov.data[:]=(pixArea)/(rmsArcmin)**2


Cluster=True

cluster_map=0

T_plot=T_map.data[:]

gibbs_tools.plot(T_plot,mapDir+'/mapPlots/'+'InitialCMB.png')



if Cluster==True:
    # Add a cluster and apply Beam + Mask

    cluster_set = ClusterSet.ClusterSet(catalogue="data/MCXC_1clu.dat", map_template=T_map)
    cluster_map = cluster_set.get_cluster_map(0, rescale=RESCALE_CLUSTER_SIZE)
    g_nu = cluster_set.tsz_spectrum(nu=150.)
    cluster_map*=g_nu
    
    T_map.data += RESCALE_TSZ_AMP * cluster_map
    
    gibbs_tools.plot(RESCALE_TSZ_AMP *cluster_map,mapDir+'/mapPlots/'+'Initialcluster.png')


T_map=gibbs_tools.applyBeam(T_map,beamTemp)

T_map=gibbs_tools.addWhiteNoise(T_map,rmsArcmin)
T_map.data[:] *= mask.data[:]
InvNoiseCov.data[:]*= mask.data[:]

T_map.writeFits(mapDir+os.path.sep+'data.fits',overWrite=True)

T_plot=T_map.data[:] * (1+numpy.log(mask.data[:]))

gibbs_tools.plot(T_plot,mapDir+'/mapPlots/'+'Initialdata.png')




cluster_amp_array=[]

for i in range(iStart,iStop):
    
        print ""
        print "Number of Gibbs step:",i
        print ""
    
        x=precond.computeX0(T_map)
    
        b=precond.computeB(T_map,InvNoiseCov,beamTemp,power_2d,cluster_map)
    
        rebuild=T_map.copy()

        t=time.time()
    
        cluster_amp=precond.preCondConjugateGradientSolver(i,b,x, power_2d,beamTemp,InvNoiseCov,cluster_map,eps=p['epsilon'],i_max=p['imax'],plotInterval=p['plotInterval'],mapDir=mapDir)
    
        print "time for a step:", time.time()-t
    
    
        cluster_amp_array+=[cluster_amp]


pylab.plot(cluster_amp_array)
pylab.show()

g = open('cluster_amp_all.dat',mode="w")
for k in xrange(len(cluster_amp_array)):
    g.write("%f\n"%(cluster_amp_array[k]))
        
g.close()


 