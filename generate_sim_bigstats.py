#!/usr/bin/python
"""
Generate a simulated CMB + SZ cluster sky.
"""
import numpy as np
import fist
from mpi4py import MPI
import os

# Load experimental and cosmology settings (p, cosmo)
import experiment
p = experiment.p
cosmo = experiment.cosmo


comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

print rank, size


iStart = p['ReaStart']
iStop = p['ReaStop']

delta = (iStop - iStart)/size

if delta == 0:
    raise ValueError, 'Too many processors for too small a  loop!'

print delta
iMin = iStart+rank*delta
iMax = iStart+(rank+1)*delta

if iMax>iStop:
    iMax = iStop
elif (iMax > (iStop - delta)) and iMax <iStop:
    iMax = iStop


# (For testing)
RESCALE_CLUSTER_SIZE = 1. # Enlarge cluster angular size by this factor
RESCALE_TSZ_AMP = 15. # Scale cluster TSZ signal amplitude by this factor


template_cmb = fist.makeEmptyCEATemplate(p['raSizeDeg'], p['decSizeDeg'],meanRa=180., meanDec=0.,pixScaleXarcmin=p['pixScaleXarcmin'],pixScaleYarcmin=p['pixScaleYarcmin'])
template= template_cmb.copy()

print p['buffer']

print '='*50

if p['buffer']==2:
    print ''
    print 'Non Periodicity'
    print ''
    template_cmb = fist.makeEmptyCEATemplate( p['buffer']*p['raSizeDeg'], p['buffer']*p['decSizeDeg'],meanRa=180., meanDec=0.,pixScaleXarcmin=p['pixScaleXarcmin'],pixScaleYarcmin=p['pixScaleYarcmin'] )

l, cl_TT = np.loadtxt(p['theoryFile']).T[:2]
cl_TT = cl_TT*2*np.pi/(l*(l+1))

power_2d = fist.make2dPowerSpectrum(template_cmb, l, cl_TT)
power_2d[0,0] = 1e-10


print "MAP PROPERTIES"
template.info()


m_dict=p['mask']
i_noise=p['inhomogeneous_noise']


if i_noise['apply']:
    print ''
    print 'Inhomogeneous noise'
    print ''
    
    weight= fist.generate_gaussian_window(template,i_noise['pad'],i_noise['fwhm'],i_noise['nSigma'])

beams = []; ninvs = []; masks = []; freqs = []

RAD2MIN = 180./np.pi * 60.
for j in range(len(p['freq'])):
                
    # Define beam shape (assumed Gaussian for now)
    #ell, bl = np.loadtxt(p['beamFile']).T
    ell = np.arange(15000)
    bl = np.exp(-ell*(ell+1)*(p['beamSizeArcmin'][j]/RAD2MIN)**2 / 8*np.log(2) )
                
    # Make 2D beam template
    beamTemp = fist.makeTemplate(template, bl, ell, np.max(l)-2, outputFile=None)
    beamTemp = beamTemp.data[:]
        
    # Build noise covmat
    pixArea = RAD2MIN**2. * template.pixScaleX * template.pixScaleY
    InvNoiseCov = template.copy()
    InvNoiseCov.data[:] = pixArea / p['rmsArcmin'][j]**2. # Noise per px
                
    if i_noise['apply']:
        InvNoiseCov.data[:]=fist.get_inv_noise_cov(weight, p['rmsArcmin'][j])
                
    # Append results to lists
    beams.append(beamTemp)
    ninvs.append(InvNoiseCov.data)
    freqs.append(p['freq'][j])



for iii in xrange(iMin,iMax):
    mapDir='sims_%03d'%iii
    
    try:
        os.makedirs(mapDir)
    except:
        pass


    # Generate a realisation of the CMB
    cmbmap = np.random.randn(template_cmb.Ny,template_cmb.Nx)
    a_l = np.fft.fft2(cmbmap)
    a_l *= np.sqrt(power_2d)
    cmbmap = np.real(np.fft.ifft2(a_l))
    
    if p['buffer'] ==2:
        template.data[:] = cmbmap[template_cmb.Ny/4:3*template_cmb.Ny/4, template_cmb.Nx/4:3*template_cmb.Nx/4]
    else:
        template.data[:] = cmbmap


    _mask = template.copy()
    _mask.data[:] = 1

    if m_dict['apply']:
        if iii==0:
            print ''
            print 'Mask'
            print ''
            print '='*50
        _mask=fist.makeMask(_mask,m_dict['nHoles'],m_dict['holeSize'],lenApodMask=0,show=False)
        _mask.data[0:p['out_pix'],:]=0
        _mask.data[_mask.Ny-p['out_pix']:_mask.Ny,:]=0
        _mask.data[:,0:p['out_pix']]=0
        _mask.data[:,_mask.Nx-p['out_pix']:_mask.Nx]=0

    _mask.writeFits(mapDir+'/mask.fits', overWrite=True)

    # Save CMB map to file
    template.writeFits(mapDir+'/cmbsim.fits', overWrite=True)

    # Plot result and save to PNG
    fist.plot(template.data*(1-np.log(_mask.data)), mapDir+'/InitialCMB.png', range=(-300., 300.))

    # Load cluster catalogue and generate cluster map
    cluster_set = fist.ClusterSet(catalogue=p['cluster_cat'], map_template=template)
    g_nu = [cluster_set.tsz_spectrum(nu=f) for f in freqs]

    clumap = np.zeros(template.data.shape)
    for i in range(cluster_set.Ncl):
        clumap += cluster_set.get_cluster_map(i, rescale=RESCALE_CLUSTER_SIZE, maptype='tsz')[0]

    # Create skymap per band
    datamaps = []
    for k in range(len(freqs)):
        print " * Simulating band", k, "(%03d GHz)" % freqs[k]    
    
        # Add amplitude-scaled cluster to map (and then plot)
        m = template.copy()
        m.data += g_nu[k] * RESCALE_TSZ_AMP * clumap
        if k == 0:
            fist.plot(RESCALE_TSZ_AMP * clumap, mapDir+'/clumap.png')

        # Convolve sky map with the beam
        m = fist.applyBeam(m, beams[k])

        # Add noise to map, and multiply by mask

        noise=np.random.randn(m.Ny,m.Nx)*ninvs[k]**(-0.5)
        m.data += noise

        #Apply The mask
#m.data *= _mask.data

        # Save datamap to FITS file, and make plots
        datamaps= [mapDir+"/data_00.fits", mapDir+"/data_01.fits"]

        m.writeFits(datamaps[k], overWrite=True)

print "Finished."
