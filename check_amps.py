#!/usr/bin/python
"""
Check for biases in the amplitude sampler by returning samples from lots of 
realisations of CMB + noise + clusters.
"""
import numpy as np
import pylab as P
import fist
import time
from mpi4py import MPI as mpi

np.random.seed(12)
comm = mpi.COMM_WORLD
rank = comm.Get_rank()

# Load experimental and cosmology settings (p, cosmo)
import experiment
p = experiment.p
cosmo = experiment.cosmo

# Make directories for maps, results etc.
mapDir = fist.check_dir_exists('sims/map')
clusterDir = fist.check_dir_exists('sims/cluster')

# Sampling settings
Nreal = 1500

# Prepare cluster map and experimental settings
expt_settings, cluster_set, clumap, g_nu = fist.prep_multiple_realisations(p, rescale_cluster_size=1.)
template, power_2d, beams, ninvs, masks, freqs = expt_settings

# Loop through realisations
chain_szamp = []
for i in range(Nreal):
    if rank == 0:
        print "\n * Realisation", i+1, "/", Nreal
        t0 = time.time()
        
        # Generate new realisation of CMB + clusters + noise
        datamaps = fist.new_realisation(p, expt_settings, clumap, g_nu, rescale_tsz_amp=5.)
        expt_setup = (datamaps, ninvs, beams, freqs)
        
        # Construct initial guess for soln. vector, RHS of linear system
        linsys_setup = fist.cg.prep_system( expt_setup, power_2d, cluster_set, 
                                            p['rmsArcmin'][0] )
        x = fist.cg.computeX0(linsys_setup)
        b = fist.cg.computeB(linsys_setup)

        # Solve system using Conjugate Gradient method
        # (Return only cluster amplitude sample as result)
        t = time.time()
        cmb_amp, sz_amp = fist.cg.preCondConjugateGradientSolver( b, x, 
                               linsys_setup, eps=p['epsilon'], i_max=p['imax'], 
                               plotInterval=p['plotInterval'], mapDir=mapDir )
        
        print "\tSZ amp.", sz_amp
        print "\tSampler took", round(time.time()-t, 3), "sec."
        
        # Save resulting SZ amplitude
        chain_szamp.append(sz_amp)
        
        for k in range(cluster_set.Ncl):
            f = open("test_amp_%03d.dat"%k, 'a')
            f.write(str(sz_amp[k]) + "\n")
            f.close()

