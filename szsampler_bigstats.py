#!/usr/bin/python
"""
Flat-sky SZ+CMB Gibbs sampler.
"""
from flipper import *
import numpy as np
import pylab as P
import fist
import time
from mpi4py import MPI 
import experiment
import os


p = experiment.p
cosmo = experiment.cosmo

# Sampling settings
Nsamp = 1
SAMPLE_AMPS = True
SAMPLE_SHAPES = False
SAMPLE_POSITIONS = False
SAMPLE_VCOV = False


comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

print rank, size



iStart = p['ReaStart']
iStop = p['ReaStop']

delta = (iStop - iStart)/size

if delta == 0:
    raise ValueError, 'Too many processors for too small a  loop!'

print delta
iMin = iStart+rank*delta
iMax = iStart+(rank+1)*delta

if iMax>iStop:
    iMax = iStop
elif (iMax > (iStop - delta)) and iMax <iStop:
    iMax = iStop


for iii in xrange(iMin,iMax):
    
    mapDir='sims_%03d'%iii
    resultDir='results_%03d'%iii
    
    try:
        os.makedirs(resultDir)
    except:
        pass

    _datamaps= [mapDir+"/data_00.fits", mapDir+"/data_01.fits"]
    datamaps = [fist.litemap_from_fits(path) for path in _datamaps]


    # Load data and instrumental specifications
    template, power_2d, beams, ninvs, freqs = fist.experiment_settings(p)

    #Apply the mask
    mask=fist.litemap_from_fits(mapDir+'/mask.fits')
#    mask.plot()

    for k in range(len(ninvs)):
        
        ninvs[k]*=mask.data[:]
        datamaps[k].data[:]*=mask.data[:]
#        datamaps[k].plot()
        T_plot =datamaps[k].data[:] * (1 + np.log(mask.data[:])) # Useful for seeing masked regions
        fist.plot(T_plot, _datamaps[k]+".png", range=(-300., 300.))



    expt_setup = (datamaps, ninvs, beams, freqs)

    # Load cluster catalogue and get initial shape parameter/position sets
    cluster_set = fist.ClusterSet(catalogue=p['cluster_cat'], map_template=datamaps[0])
    shape = cluster_set.get_all_profile_params()
    pos = cluster_set.get_all_positions()

    # Define proposal covmat for shape sampler
    # (0:alpha, 1:beta, 2:gamma, 3:c500, 4:P0, 5:M500, 6:r500, 7:z)
    shape_cov = np.zeros((8,8))
    shape_cov[6,6] = (0.0005)**2. # r500

    # Define proposal covmat for position sampler
    # (0:l, 1:b)
    pos_cov = np.zeros((2,2))
    pos_cov[0,0] = pos_cov[1,1] = (0.0002)**2.

    # KSZ velocity covariance matrix
    vcov = (300.**2.) * np.eye(cluster_set.Ncl) # FIXME: Just made this up
    vcov = None # To disable KSZ sampling

    # Prepare linear system for amplitude sampler
    if SAMPLE_AMPS:
        linsys_setup = fist.cg.prep_system(expt_setup, power_2d, cluster_set,p['rmsArcmin'][0], vcov=vcov)

    # Keep track of parameters as MCMC progresses
    chain_tszamp = []
    chain_kszamp = []
    chain_shape = []
    chain_pos = []

    # Main Gibbs loop
    for i in range(Nsamp):

        # ==========================================================================
        # (1) AMPLITUDE SAMPLER
        # ==========================================================================
        if SAMPLE_AMPS:

                print "    Sampling amplitudes..."

                # Construct initial guess for soln. vector, RHS of linear system
                x = fist.cg.computeX0(linsys_setup)
                b = fist.cg.computeB(linsys_setup)

                # Solve system using Conjugate Gradient method
                # (Return only cluster amplitude sample as result)
                t = time.time()
            
                cmb_amp, mono,  tsz_amp, ksz_amp = fist.cg.preCondConjugateGradientSolver(b, x, linsys_setup, eps=p['epsilon'],i_max=p['imax'], plotInterval=p['plotInterval'],mapDir=mapDir )
            
                cmb=template.copy()
                cmb.data[:]=cmb_amp
                
                cmb.writeFits(mapDir+'/recovered_cmb_%03d.fits'%i,overWrite=True)
                    
                print '\tMonopole',mono
            
                power_spec=fftTools.powerFromLiteMap(cmb)
                l_low,l_up,l,cl,std,w=power_spec.binInAnnuli(p['binningFile'])
                id= numpy.where(l< 6000)
                l,cl= l[id],cl[id]

                g = open(resultDir+"/ps_%03d.dat"%i,mode="w")

                for k in xrange(len(l)):
                    g.write("%f %e %e\n"%(l[k],cl[k],w[k]))
                g.close()
    
                if vcov is not None:
                    print "\tTSZ/KSZ amp.", tsz_amp, ksz_amp
                else:
                    print "\tTSZ amp.", tsz_amp
                print "\tSampler took", round(time.time()-t, 3), "sec."
            
                cmb_amp = cmb_amp.flatten() # Prepare to send via MPI

                cmb_amp = np.reshape(cmb_amp, template.data.shape)
        
                # Append results to file
                chain_tszamp.append(tsz_amp)
                for k in range(cluster_set.Ncl):
                    f = open(resultDir+"/chain_tsz_%03d.dat"%k, 'a')
                    f.write(str(tsz_amp[k]) + "\n")
                    f.close()
    
