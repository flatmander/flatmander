#!/usr/bin/python

import numpy as np
from mpi4py import MPI as mpi

comm = mpi.COMM_WORLD
rank = comm.Get_rank()

Nparam = 5
Nclu = 7

def idxs_for_worker(N, rank=None, comm=None):
    """
    Return a list of indices for an array of length N, evenly divided up 
    between workers.
    """
    if comm is None:
        return range(N)
    idxs = range(rank, N, comm.size)
    return idxs


params = []
for i in idxs_for_worker(Nclu, rank, comm):
    params.append(i*np.arange(Nparam))


# Pull it all back to root
new_params = comm.allgather(params)

p = [[]]*Nclu


if rank == 0:
  for i in range(comm.size):
    myidxs = idxs_for_worker(Nclu, i, comm)
    for j in range(len(myidxs)):
      p[myidxs[j]] = new_params[i][j]

if rank == 0:
    for _p in p:
        print _p
