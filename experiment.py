
mapDir = "sims/map"

p = {
    'raSizeDeg':        4.27, # 256 px
    'decSizeDeg':       4.27,
    'pixScaleXarcmin':  1,
    'pixScaleYarcmin':  1,
    'rmsArcmin':        [4., 3.],
    'beamSizeArcmin':   [1.5, 1.5],
    'freq':             [100., 150.],
    'datamaps':         [mapDir+"/data_00.fits", mapDir+"/data_01.fits"],
    'beamFile':         'data/beams_AR1_2009_120815.dat',
    'theoryFile':       'data/bode_almost_wmap5_lmax_1e4_lensedCls.dat',
    'cluster_cat':      'data/MCXC_1clu.dat',
    'ReaStart':         0,
    'ReaStop':          1,
    'iStart':           0,
    'iStop':            10,
    'epsilon':          1e-11,
    'plotInterval':     100,
    'imax':             500,
    'buffer':           2, # Increase 
    'out_pix':          6,
    'inhomogeneous_noise': {'apply': False, 'pad': 0.25, 'fwhm':25, 'nSigma':2.0},
    'mask':             {'apply':True, 'nHoles':15, 'holeSize':5, 'LenApodMask':50},
    'binningFile':      'data/binningTest'
}

cosmo = {
    'omega_M_0': 		0.3,
    'omega_lambda_0': 	0.7,
    'omega_b_0': 		0.045,
    'omega_n_0':		0.0,
    'omega_k_0':		0.0,
    'N_nu':			    0,
    'h':				0.7,
    'n':				0.96,
    'sigma_8':			0.8,
    'baryonic_effects': True
}

